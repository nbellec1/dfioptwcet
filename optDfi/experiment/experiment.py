# coding=utf-8
from contextlib import contextmanager
from time import time
from shutil import copyfile

from pathlib import Path
from typing import List, Literal, Optional, TypeAlias
import subprocess as sp
import logging

from configurations import config
from optDfi.experiment.CSVResult import CSVResult
from optDfi.experiment.timing import time_into_csv
from optDfi.optimizations.ilp_opt.tag_check_ilp_opt import optimize_wcep_ilp, EmptyProblemException, \
    UnsolvedILPException
from optDfi.optimizations.improve_reachability_analysis import improve_reachability_analysis
from pyDFI.compile import execute_aiT, execute_comet
from optDfi.systemInteraction.DFILib import execute_clang, execute_opt, execute_phasar, execute_llc, execute_gcc, \
    execute_qemu
from optDfi.experiment.FileSystem import FileSystem
from optDfi.misc.utils import Color, color_str
from pyDFI.exceptions import FileNotFoundException
from optDfi.experiment.Benchmark import Benchmark

TimingTy: TypeAlias = Literal[ "witness_build",
"witness_wcet",
"sota_build",
"sota_wcet",
"pre_value_build",
"pre_value_wcet",
"value_analysis",
"value_analysis_build",
"value_analysis_wcet",
"ilp_0_analysis",
"ilp_0_build",
"ilp_0_wcet",
"ilp_1_analysis",
"ilp_1_build",
"ilp_1_wcet",
"ilp_2_analysis",
"ilp_2_build",
"ilp_2_wcet",
"ilp_3_analysis",
"ilp_3_build",
"ilp_3_wcet" ]

ResultTy: TypeAlias = Literal[ "witness_wcet",
"sota_wcet",
"value_analysis_wcet",
"ilp_0_wcet",
"ilp_1_wcet",
"ilp_2_wcet",
"ilp_3_wcet",
"it_num"
]

lschain_opts = {
    "wordChain" : "word-chain",
    "halfChain" : "halfword-chain",
    "byteChain" : "byte-chain",
    "sandboxElim" : "sandbox-elim",
    "chainCut" : "chain-cut"
}


# Building part
def build_common( bench: Benchmark, fs: FileSystem ) :
    """
    Generate common files for a given benchmark.
    Common files are the files shared by all compilation of this bench, such as the
    combined IR and the first O1 + alignement pass inside opt
    @param bench:
    @param fs:
    """
    execute_clang( sources=bench.sources,
                   output_file=fs.linked_ll,
                   includes=bench.includes,
                   flags=bench.clang_flags )

    passes = [ "O1", "dfialignment" ] if bench.tag != "susan" else [ "dfialignment" ]
    execute_opt( entry_file=fs.linked_ll,
                 output_file=fs.aligned_ll,
                 passes=passes,  # the O1 can generate errors on some benchmarks such as susan
                 flags=bench.opt_flags )


def build_IR_to_exes( ll_file: Path, asm_file: Path, exe_comet_file: Path,
                      exe_qemu_file: Path,
                      llc_flags: List[ str ], gcc_flags: List[ str ] ) :
    """
    Build the IR file (after any optimization) into 2 RISC-V executable (one for QEMU and one for COMET)
    @param ll_file:
    @param asm_file:
    @param exe_comet_file:
    @param exe_qemu_file:
    @param llc_flags:
    @param gcc_flags:
    """
    execute_llc( entry_file=ll_file,
                 output_file=asm_file,
                 flags=llc_flags )

    # Comet
    execute_gcc( entry_file=asm_file,
                 output=exe_comet_file,
                 linker_script=config.LINKER_SCRIPTS[ "comet" ],
                 flags=gcc_flags )

    # QEMU/WCET
    execute_gcc( entry_file=asm_file,
                 output=exe_qemu_file,
                 linker_script=config.LINKER_SCRIPTS[ "qemu-wcet" ],
                 flags=gcc_flags )


def build_witness( bench: Benchmark, fs: FileSystem ) :
    """
    Build the WITNESS executable, i.e. the one without DFI
    @param bench:
    @param fs:
    """
    logging.info( color_str( "Compiling WITNESS", Color.ORANGE ) )

    build_IR_to_exes(
        ll_file=fs.aligned_ll,
        asm_file=fs.witness_asm,
        exe_qemu_file=fs.witness_qemu_exe_path,
        exe_comet_file=fs.witness_comet_exe_path,
        llc_flags=bench.llc_flags,
        gcc_flags=bench.gcc_flags
    )


def build_dfi_prebackend( bench: Benchmark, fs: FileSystem ) :
    """
    Transform the IR to perform the analysis on it and optimize it with the current SotA optimization
    (presented in Castro et al.)
    @param bench:
    @param fs:
    """
    execute_opt( entry_file=fs.aligned_ll,
                 output_file=fs.annotated_ll,
                 passes=[ "dfiannotation" ],  # -O1 a un bug pour 1 benchmark
                 flags=bench.opt_flags )

    execute_phasar( entry_file=fs.annotated_ll,
                    output_file=fs.analysed_ll )

    logging.info( color_str( "Starting optimization", Color.BLUE ) )
    execute_opt( entry_file=fs.analysed_ll,
                 output_file=fs.iteration_optimized_ll,
                 passes=bench.opt_passes + [ "dfiintrinsicinstr" ],
                 flags=bench.opt_flags )


def build_dfi_protected( bench: Benchmark, fs: FileSystem, bonus_llc_flags: Optional[ List[ str ] ] = None ) :
    """
    Transform IR with DFI annotations into 2 RISC-V DFI protected executables
    @param bench:
    @param fs:
    @param bonus_llc_flags:
    """
    if bonus_llc_flags is None :
        bonus_llc_flags = [ ]

    logging.info( color_str( "Compiling DFI", Color.ORANGE ) )
    build_IR_to_exes(
        ll_file=fs.iteration_optimized_ll,
        asm_file=fs.iteration_asm,
        exe_qemu_file=fs.it_bin_qemu,
        exe_comet_file=fs.it_bin_comet,
        llc_flags=bench.llc_flags + [ '--emit-dfi' ] + bonus_llc_flags,
        gcc_flags=bench.gcc_flags
    )


def gen_ais_file( entry_file: Path, output_file: Path, executable: Path, dfi: bool ) :
    """
    Generate AIS annotation files for AiT.
    In particular, this files contains a few annotations in case of DFI protected
    executable to prevent aiT from considering DFI alarm as normal execution
    @param entry_file:
    @param output_file:
    @param executable:
    @param dfi:
    """
    if not entry_file.exists() :
        raise FileNotFoundException( entry_file )
    if not executable.exists() :
        raise FileNotFoundException( executable )

    with open( output_file, "w" ) as output :
        output.write( "try {\n" )

        with open( entry_file, "r" ) as entry :
            output.writelines( entry.readlines() )

    objdump = config.DEFAULT_RISCV32_OBJDUMP_PATH

    if dfi :
        ebreak_inf_cmd1 = f"{objdump} -d {executable}"
        ebreak_inf_cmd2 = "| grep \"ebreak\" | awk '{ sub(\":\", \"\", $1); print \"instruction 0x\" $1 \"-0x4 condition always taken;\" }'"
        ebreak_inf_cmd3 = f">> {output_file}"

        ebreak_inf_cmd = " ".join(
            [ ebreak_inf_cmd1, ebreak_inf_cmd2, ebreak_inf_cmd3 ] )

        p = sp.run( ebreak_inf_cmd, shell=True, stdout=sp.PIPE,
                    text=True, encoding='utf-8' )

        if p.returncode != 0 :
            raise Exception( "Failed to execute the objdump command" )

    with open( output_file, "a" ) as f :
        f.write( "\nsuppress message: 3103;\n" )
        f.write( "}" )


def gen_apx_files( executable: Path, ais_file: Path, output_file: Path, xml_file: Path, txt_report: Path,
                   entry_point: str = "main" ) :
    """
    Generate the APX file for AiT that describe which executable must be estimated, with which entry point,
    which annotation file and where to store the result
    @param executable:
    @param ais_file:
    @param output_file:
    @param xml_file:
    @param txt_report:
    @param entry_point:
    """
    template: Optional[ str ] = None

    with open( config.APX_TEMPLATE_PATH, "r" ) as f :
        template = "".join( f.readlines() )

    apx = template.format(
        executable=executable,
        xml_report=xml_file,
        ais=ais_file,
        txt_report=txt_report,
        analysis_start=entry_point
    )

    with open( output_file, "w" ) as f :
        f.write( apx )


def prepare_aiT_witness_files( bench: Benchmark, fs: FileSystem ) :
    gen_ais_file( bench.ais_file, fs.witness_ais,
                  fs.witness_qemu_exe_path, dfi=False )
    gen_apx_files( fs.witness_qemu_exe_path, fs.witness_ais, fs.witness_apx, fs.witness_xml, fs.witness_txt,
                   bench.entry_point )


def prepare_aiT_dfi_files( bench: Benchmark, fs: FileSystem, entry_point: str ) :
    gen_ais_file( entry_file=bench.ais_file,
                  output_file=fs.it_aiT_ais,
                  executable=fs.it_bin_qemu,
                  dfi=True )
    gen_apx_files( executable=fs.it_bin_qemu,
                   ais_file=fs.it_aiT_ais,
                   output_file=fs.it_aiT_apx,
                   xml_file=fs.it_aiT_xml,
                   txt_report=fs.it_aiT_txt,
                   entry_point=entry_point )


def build_benchmark( bench: Benchmark, fs: FileSystem ) :
    build_common( bench, fs )

    fs_sota = FileSystem( fs.base_dir, fs.bench_name, 0 )
    build_dfi_prebackend( bench, fs_sota )
    build_dfi_protected( bench, fs_sota )


def experiment_iteration_vs_witness(
        bench: Benchmark,
        fs: FileSystem,
        result_csv: CSVResult[ ResultTy, int ],
        timing_csv: CSVResult[ TimingTy, float ],
        time_limit: int,
        slack: int,
        number_iteration: int
) :
    """
    Perform the experiment of comparing SotA DFI against DFI optimized with our iterative, 
    WCET-oriented optimization,
    reworked for clarity
    @param bench:
    @param fs:
    @param result_file:
    @param timing_file:
    @param time_limit:
    @param slack:
    @param number_iteration:
    """

    # Build the files common to witness and DFI protected executables
    build_common( bench, fs )

    # Build the witness (and time it)
    with time_into_csv( timing_csv, bench.tag, "witness_build" ) :
        build_witness( bench, fs )

    # Estimates the WCET of the witness
    with time_into_csv( timing_csv, bench.tag, "witness_wcet" ) :
        prepare_aiT_witness_files( bench, fs )
        witness_wcet = execute_aiT(
            entry_file=fs.witness_qemu_exe_path,
            apx_file=fs.witness_apx,
            xml_file=fs.witness_xml
        )

    # Add the witness WCET to the results
    result_csv.add_data( bench.tag, "witness_wcet", witness_wcet )

    # Prepare the FileSystem for the SotA DFI protected executables
    fs_sota = FileSystem( fs.base_dir, fs.bench_name, 0 )

    # Build the SotA DFI protected executables
    with time_into_csv( timing_csv, bench.tag, "sota_build" ) :
        build_dfi_prebackend( bench, fs_sota )
        build_dfi_protected( bench, fs_sota )

    # Estimates the WCET of the SotA
    with time_into_csv( timing_csv, bench.tag, "sota_wcet" ) :
        prepare_aiT_dfi_files( bench, fs_sota, entry_point=bench.entry_point )
        sota_wcet = execute_aiT( entry_file=fs_sota.it_bin_qemu,
                                 apx_file=fs_sota.it_aiT_apx,
                                 xml_file=fs_sota.it_aiT_xml )

    # Add the SotA WCET to the results
    result_csv.add_data( bench.tag, "sota_wcet", sota_wcet )

    # Copy the SotA executable to perform the value analysis of AiT on the whole program
    # (not just the entry point of the bench)

    # TODO: Could be optimized by copying directly the executable and re-launching AiT on it
    fs_pre_value_analysis = FileSystem( fs.base_dir, fs.bench_name, 1 )
    copyfile( fs_sota.iteration_optimized_ll,
              fs_pre_value_analysis.iteration_optimized_ll )
    fs_value_analysis = FileSystem( fs.base_dir, fs.bench_name, 2 )

    # Time the build pre-value analysis
    with time_into_csv( timing_csv, bench.tag, "pre_value_build" ) :
        build_dfi_protected( bench, fs_pre_value_analysis )

    # Time the WCET estimation (even if we are only interested in the value analysis results)
    with time_into_csv( timing_csv, bench.tag, "pre_value_wcet" ) :
        prepare_aiT_dfi_files( bench, fs_pre_value_analysis, entry_point="main" )

        execute_aiT( entry_file=fs_pre_value_analysis.it_bin_qemu,
                     apx_file=fs_pre_value_analysis.it_aiT_apx,
                     xml_file=fs_pre_value_analysis.it_aiT_xml )

    # Time the time required to optimize the program using the value analysis
    with time_into_csv( timing_csv, bench.tag, "value_analysis" ) :
        improve_reachability_analysis( xml_file=fs_pre_value_analysis.it_aiT_xml,
                                       binary_file=fs_pre_value_analysis.it_bin_qemu,
                                       original_ll_file=fs_pre_value_analysis.iteration_optimized_ll,
                                       optimized_ll_file=fs_value_analysis.iteration_optimized_ll,
                                       log_file=fs_value_analysis.it_problem_log )

    # Time and build the optimized with value analysis DFI protected executables
    with time_into_csv( timing_csv, bench.tag, "value_analysis_build" ) :
        build_dfi_protected( bench, fs_value_analysis )

    # Estimate the WCET post value analysis optimization
    with time_into_csv( timing_csv, bench.tag, "value_analysis_wcet" ) :
        prepare_aiT_dfi_files( bench, fs_value_analysis,
                               entry_point=bench.entry_point )

        value_analysis_wcet = execute_aiT( entry_file=fs_value_analysis.it_bin_qemu,
                                           apx_file=fs_value_analysis.it_aiT_apx,
                                           xml_file=fs_value_analysis.it_aiT_xml )

    # Add the WCET post value analysis to the results
    result_csv.add_data( bench.tag, "value_analysis_wcet", value_analysis_wcet )

    solution = None
    previousItFs = fs_value_analysis

    wcets = [ value_analysis_wcet ]
    try :
        # We now iterates up to the maximum number of iteration and perform our iterative optimization
        for it_num in range( number_iteration ) :
            iteration = fs_value_analysis.it_num + 1 + it_num
            nextItFs = FileSystem( fs.base_dir, fs.bench_name, iteration )

            # Optimize the previous iteration based on its WCEP
            with time_into_csv( timing_csv, bench.tag, f"ilp_{it_num}_analysis" ) :
                solution = optimize_wcep_ilp(
                    xml_file=previousItFs.it_aiT_xml,
                    binary_file=previousItFs.it_bin_qemu,
                    original_ll_file=previousItFs.iteration_optimized_ll,
                    optimized_ll_file=nextItFs.iteration_optimized_ll,
                    previous_solution=solution,
                    problem_log=nextItFs.it_problem_log,
                    ilp_log=nextItFs.it_ilp_log,
                    time_limit=time_limit,
                    slack=slack )

            # Build the new executable
            with time_into_csv( timing_csv, bench.tag, f"ilp_{it_num}_build" ) :
                build_dfi_protected( bench, nextItFs )

            # Compute the new executable WCET
            with time_into_csv( timing_csv, bench.tag, f"ilp_{it_num}_wcet" ) :
                prepare_aiT_dfi_files(
                    bench, nextItFs, entry_point=bench.entry_point )

                wcet = execute_aiT( entry_file=nextItFs.it_bin_qemu,
                                    apx_file=nextItFs.it_aiT_apx,
                                    xml_file=nextItFs.it_aiT_xml )

            wcets.append( wcet )
            previousItFs = nextItFs

    except EmptyProblemException :
        logging.warning( color_str( "Stopped due to empty problem", Color.RED ) )
    except UnsolvedILPException :
        logging.error( color_str( "Stopped due to unresolved ILP Exception", Color.RED ) )

    first_wcet = wcets[ 1 ] if len( wcets ) >= 2 else wcets[ 0 ]
    best_wcet = min( wcets )

    logging.info( color_str(
        f"{bench.tag:15}:{witness_wcet:10}|{sota_wcet:10}|{value_analysis_wcet:10}|{first_wcet:10}|{best_wcet:10}",
        color=Color.GREEN ) )

    # Add the WCET post value analysis to the results
    for i in range( number_iteration ) :
        result_csv.add_data(
            bench.tag, f"ilp_{i}_wcet", wcets[ i + 1 ] if len( wcets ) >= 2 else wcets[ 0 ] )

    result_csv.add_data( bench.tag, "it_num", it_num + 1 )


def rtdfi_and_lschain( bench: Benchmark, fs: FileSystem, result_csv: CSVResult, time_limit: int,
                       lschain_additive_passes: List[ str ] ) :
    # Build the files common to witness and DFI protected executables
    build_common( bench, fs )
    build_witness( bench, fs )

    # Estimates the WCET of the witness
    prepare_aiT_witness_files( bench, fs )
    witness_wcet = execute_aiT(
        entry_file=fs.witness_qemu_exe_path,
        apx_file=fs.witness_apx,
        xml_file=fs.witness_xml
    )

    # Add the witness WCET to the results
    result_csv.add_data( bench.tag, "witnessWCET", witness_wcet )

    iteration = 0

    # Prepare the FileSystem for the SotA DFI protected executables
    fs_sota = FileSystem( fs.base_dir, fs.bench_name, 0 )
    iteration += 1

    # Build the common to all DFI protected part
    build_dfi_prebackend( bench, fs_sota )

    # Build the SotA
    build_dfi_protected( bench, fs_sota )

    # Estimates the WCET of the SotA
    prepare_aiT_dfi_files( bench, fs_sota, entry_point=bench.entry_point )
    sota_wcet = execute_aiT( entry_file=fs_sota.it_bin_qemu,
                             apx_file=fs_sota.it_aiT_apx,
                             xml_file=fs_sota.it_aiT_xml )

    # Add the SotA WCET to the results
    result_csv.add_data( bench.tag, "sotaWCET", sota_wcet )

    fs_pre_value_analysis = FileSystem( fs.base_dir, fs.bench_name, iteration )
    iteration += 1
    copyfile( fs_sota.iteration_optimized_ll,
              fs_pre_value_analysis.iteration_optimized_ll )
    fs_value_analysis = FileSystem( fs.base_dir, fs.bench_name, iteration )
    iteration += 1

    # Build pre-value analysis
    build_dfi_protected( bench, fs_pre_value_analysis ) #bonus_llc_flags=[ lschains_flags ] )

    # WCET estimation (even if we are only interested in the value analysis results)
    prepare_aiT_dfi_files( bench, fs_pre_value_analysis, entry_point="main" )
    execute_aiT( entry_file=fs_pre_value_analysis.it_bin_qemu,
                 apx_file=fs_pre_value_analysis.it_aiT_apx,
                 xml_file=fs_pre_value_analysis.it_aiT_xml )

    # Time the time required to optimize the program using the value analysis
    improve_reachability_analysis( xml_file=fs_pre_value_analysis.it_aiT_xml,
                                   binary_file=fs_pre_value_analysis.it_bin_qemu,
                                   original_ll_file=fs_pre_value_analysis.iteration_optimized_ll,
                                   optimized_ll_file=fs_value_analysis.iteration_optimized_ll,
                                   log_file=fs_value_analysis.it_problem_log )

    # Build the optimized with value analysis DFI protected executables
    build_dfi_protected( bench, fs_value_analysis) #, bonus_llc_flags=[ lschains_flags ] )

    # Estimate the WCET post value analysis optimization
    prepare_aiT_dfi_files( bench, fs_value_analysis,
                           entry_point=bench.entry_point )
    value_analysis_wcet = execute_aiT( entry_file=fs_value_analysis.it_bin_qemu,
                                       apx_file=fs_value_analysis.it_aiT_apx,
                                       xml_file=fs_value_analysis.it_aiT_xml )

    # Add the WCET post value analysis to the results
    result_csv.add_data( bench.tag, "vaWCET", value_analysis_wcet )

    solution = None
    previousItFs = fs_value_analysis
    wcets = [ value_analysis_wcet ]

    nextItFs = FileSystem( fs.base_dir, fs.bench_name, iteration )
    iteration += 1

    try :
        # Optimize the previous iteration based on its WCEP
        solution = optimize_wcep_ilp(
            xml_file=previousItFs.it_aiT_xml,
            binary_file=previousItFs.it_bin_qemu,
            original_ll_file=previousItFs.iteration_optimized_ll,
            optimized_ll_file=nextItFs.iteration_optimized_ll,
            previous_solution=solution,
            problem_log=nextItFs.it_problem_log,
            ilp_log=nextItFs.it_ilp_log,
            time_limit=time_limit,
            slack=0 )

        # Build the new executable
        build_dfi_protected( bench, nextItFs ) #, bonus_llc_flags=[ lschains_flags ] )

        # Compute the new executable WCET
        prepare_aiT_dfi_files(
            bench, nextItFs, entry_point=bench.entry_point )

        wcet = execute_aiT( entry_file=nextItFs.it_bin_qemu,
                            apx_file=nextItFs.it_aiT_apx,
                            xml_file=nextItFs.it_aiT_xml )

        wcets.append( wcet )

    except EmptyProblemException :
        logging.warning( "Stopped due to empty problem" )
        copyfile( previousItFs.iteration_optimized_ll,
                  nextItFs.iteration_optimized_ll )
    except UnsolvedILPException :
        logging.error( "Stopped due to unresolved ILP Exception" )
        copyfile( previousItFs.iteration_optimized_ll,
                  nextItFs.iteration_optimized_ll )

    rtdfi_wcet = wcets[ -1 ]

    # Add the WCET post value analysis to the results
    result_csv.add_data( bench.tag, f"rtdfiWCET", rtdfi_wcet )

    for i in range( len( lschain_additive_passes ) ) :
        fs_lschain = FileSystem( fs.base_dir, fs.bench_name, iteration )
        iteration += 1
        lschains_flags = f"--dfi-lschain-opts={','.join( (lschain_opts[ passK ] for passK in lschain_additive_passes[ :i + 1 ]) )}"

        add_ll_flags = [ lschains_flags ]

        copyfile( nextItFs.iteration_optimized_ll,
                  fs_lschain.iteration_optimized_ll )
        build_dfi_protected( bench, fs_lschain, bonus_llc_flags=add_ll_flags )

        if not execute_qemu( fs_lschain.it_bin_qemu, fs_lschain.iteration_qemu_output ) :
            raise Exception( f"QEMU failed to execute {bench.tag} LSCHAIN pass {lschain_additive_passes[ i ]}" )

        prepare_aiT_dfi_files( bench, fs_lschain, entry_point=bench.entry_point )
        lschain_wcet = execute_aiT( entry_file=fs_lschain.it_bin_qemu,
                                    apx_file=fs_lschain.it_aiT_apx,
                                    xml_file=fs_lschain.it_aiT_xml )

        result_csv.add_data( bench.tag, f"lschain{lschain_additive_passes[ i ]}WCET", lschain_wcet )


def experiment_lschain( bench: Benchmark, fs: FileSystem, result_csv: CSVResult, lschain_additive_passes: List[ str ],
                        compute_wcet: bool = False, compute_comet: bool = True ) :
    # We build all the required executables
    build_common( bench, fs )
    build_witness( bench, fs )

    if not execute_qemu( fs.witness_qemu_exe_path, fs.witness_qemu_output ) :
        raise Exception( f"QEMU failed to execute {bench.tag} witness" )

    if compute_wcet :
        prepare_aiT_witness_files( bench, fs )
        witness_wcet = execute_aiT( entry_file=fs.witness_qemu_exe_path,
                                    apx_file=fs.witness_apx,
                                    xml_file=fs.witness_xml )

        result_csv.add_data( bench.tag, "witnessWCET", witness_wcet )

    if compute_comet:
        witness_time = execute_comet( entry_file=fs.witness_comet_exe_path )
        result_csv.add_data( bench.tag, "witnessTime", witness_time )

    fs_sota = FileSystem( fs.base_dir, fs.bench_name, 0 )
    build_dfi_prebackend( bench, fs_sota )
    build_dfi_protected( bench, fs_sota )

    if not execute_qemu( fs_sota.it_bin_qemu, fs_sota.iteration_qemu_output ) :
        raise Exception( f"QEMU failed to execute {bench.tag} SOTA" )

    if compute_wcet :
        prepare_aiT_dfi_files( bench, fs_sota, entry_point=bench.entry_point )
        sota_wcet = execute_aiT( entry_file=fs_sota.it_bin_qemu,
                                 apx_file=fs_sota.it_aiT_apx,
                                 xml_file=fs_sota.it_aiT_xml )

        result_csv.add_data( bench.tag, "sotaWCET", sota_wcet )

    if compute_comet:
        sota_time = execute_comet( entry_file=fs_sota.it_bin_comet )
        result_csv.add_data( bench.tag, "sotaTime", sota_time )

    for i in range( len( lschain_additive_passes ) ) :
        fs_lschain = FileSystem( fs.base_dir, fs.bench_name, i + 1 )
        lschains_flags = f"--dfi-lschain-opts={','.join( (lschain_opts[ passK ] for passK in lschain_additive_passes[ :i + 1 ]) )}"

        add_ll_flags = [ lschains_flags ]

        copyfile( fs_sota.iteration_optimized_ll,
                  fs_lschain.iteration_optimized_ll )
        build_dfi_protected( bench, fs_lschain, bonus_llc_flags=add_ll_flags )

        if not execute_qemu( fs_lschain.it_bin_qemu, fs_lschain.iteration_qemu_output ) :
            raise Exception( f"QEMU failed to execute {bench.tag} LSCHAIN pass {lschain_additive_passes[ i ]}" )

        if compute_wcet :
            prepare_aiT_dfi_files( bench, fs_lschain, entry_point=bench.entry_point )
            lschain_wcet = execute_aiT( entry_file=fs_lschain.it_bin_qemu,
                                        apx_file=fs_lschain.it_aiT_apx,
                                        xml_file=fs_lschain.it_aiT_xml )

            result_csv.add_data( bench.tag, f"lschain{lschain_additive_passes[ i ]}WCET", lschain_wcet )

        if compute_comet:
            lschain_time = execute_comet( entry_file=fs_lschain.it_bin_comet )
            result_csv.add_data( bench.tag, f"{lschain_additive_passes[ i ]}Time", lschain_time )
