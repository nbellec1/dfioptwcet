# coding=utf-8

from dataclasses import dataclass
from pathlib import Path
from typing import List


@dataclass
class Benchmark( object ) :
    sources: List[ Path ]
    includes: List[ Path ]
    tag: str

    clang_flags: List[ str ]

    opt_flags: List[ str ]
    opt_passes: List[ str ]

    llc_flags: List[ str ]
    gcc_flags: List[ str ]

    ais_file: Path
    entry_point: str


def findCFiles( source_dir: Path ) -> List[ Path ] :
    return [ f.resolve().absolute() for f in source_dir.iterdir() if f.suffix == ".c" ]
