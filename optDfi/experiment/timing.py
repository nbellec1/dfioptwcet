# coding=utf-8

from typing import Callable, TypeVar, Tuple, ParamSpec
from time import time
from contextlib import contextmanager

from optDfi.experiment.CSVResult import CSVResult


T = ParamSpec('T')
V = TypeVar('V')

def time_fct( fct: Callable[T, V], *args: T.args, **kwargs: T.kwargs ) -> Tuple[float, V]:
    before_time = time()
    res = fct( *args, **kwargs )
    after_time = time()

    diff_time = after_time - before_time
    
    return diff_time, res

@contextmanager
def time_into_csv(csvRes: CSVResult, bench: str, charac: str):
    if charac not in csvRes.characteristics:
        raise Exception(f"Characteristic {charac} is not a characteristic of csvRes")
    
    if bench not in csvRes.bench:
        raise Exception(f"Benchmark {bench} not in csvRes")

    try:
        start_time = time()
        yield None
    finally:
        end_time = time()
        csvRes.add_data(bench, charac, end_time-start_time)
