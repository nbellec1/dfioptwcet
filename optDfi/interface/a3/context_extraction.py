# coding=utf-8
from __future__ import annotations
import dataclasses
import time
import xml.etree.ElementTree as ET
from pathlib import Path
from typing import List, Tuple, Dict
import pipe

from optDfi.interface.a3.misc import NS, XPATH_WCET_PATH, XPATH_VALUE_BLOCK

SOURCE_BLOCK = str
SOURCE_CONTEXT = str
COUNT = int
WCEP_CONTEXT = Tuple[ SOURCE_BLOCK, SOURCE_CONTEXT, COUNT ]
LOAD_ADD_HEX = str
CONTEXT_ID = str
TAG = int

Time = {
    "val_instruction": 0,
    "count": 0,
    "extract_tag": 0,
}

@dataclasses.dataclass
class A3ValueAnalysisContext( object ) :
    context_id: CONTEXT_ID
    tags: List[ TAG ]
    count: COUNT


A3_VALUE_ANALYSIS = Dict[ LOAD_ADD_HEX, List[ A3ValueAnalysisContext ] ]


@dataclasses.dataclass
class WCEPContext( object ) :
    source_block: SOURCE_BLOCK
    source_context: SOURCE_CONTEXT
    count: COUNT


XPATH_DFILOAD_VALUE_STEP = "a3:value_step[@type=\"read\"][@width=\"2\"]"


def extract_wcep_context_from_xml( xml_file: Path ) -> List[ WCEPContext ] :
    """ Extract all the contexts in the WCEP """

    xml_root = ET.parse( xml_file ).getroot()
    xml_path = f"./{XPATH_WCET_PATH}/a3:wcet_routine/a3:wcet_context/a3:wcet_edge"
    wcep_edges = xml_root.findall( xml_path, NS )

    wcep_contexts = list(
        wcep_edges
        | pipe.map( lambda edge :
                    WCEPContext(
                        source_block=edge.get( "source_block", None ),
                        source_context=edge.get( "source_context", None ),
                        count=int( edge.get( "count", "-1" ) )
                    ) )
        | pipe.where( lambda c : c.source_block is not None and c.source_context is not None and c.count != -1 )
    )

    return wcep_contexts


def extract_tags_from_val_insts( value_instructions: List[ ET.Element ], context_id: str,
                                 data: A3_VALUE_ANALYSIS, count: int = 1 ) :
    """ Utilitary function to reduce duplicate code (currently works by modifying data with side-effect)"""
    for inst in value_instructions :
        xml_value_step = f"./{XPATH_DFILOAD_VALUE_STEP}"
        val_steps = inst.findall( xml_value_step, NS )
        inst_address = inst.get( "address" )

        tags = [ ]

        if len( val_steps ) == 0 :
            print( "Error with value step" )
            continue

        for step in val_steps :
            xml_content = "./a3:value_step_content/a3:value_interval"
            val_intervals = step.findall( xml_content, NS )

            for interval in val_intervals :
                min_tag = int( interval.get( "min" ), base=16 )
                max_tag = int( interval.get( "max" ), base=16 )
                mod = int( interval.get( "mod", default="0" ) )
                rem = int( interval.get( "rem", default="0x0" ), base=16 )

                if min_tag % (2 ** mod) == rem :
                    for i in range( min_tag, max_tag + 1, 2 ** mod ) :
                        tags.append( i )
                else :
                    min_tag = min_tag - (min_tag % (2 ** mod)) + rem
                    for i in range( min_tag, max_tag + 1, 2 ** mod ) :
                        tags.append( i )

        if inst_address not in data :
            data[ inst_address ] = [ ]

        data[ inst_address ].append( A3ValueAnalysisContext(context_id, [ tag for tag in tags ], count) )


def extract_tag_from_wcep_context( xml_file: Path, wcep_contexts: List[ WCEPContext ] ) -> A3_VALUE_ANALYSIS :
    """ Function used to retrieve data from the value analysis about the WCEP"""
    global Time

    xml_root = ET.parse( xml_file ).getroot()

    data: A3_VALUE_ANALYSIS = { }

    value_block_dict = { }

    for value_block in xml_root.findall( f"{XPATH_VALUE_BLOCK}", NS ):
        vblock_bbid = value_block.get( "id" )
        vblock_cid = value_block.get( "context_id" )
        value_block_dict[(vblock_bbid, vblock_cid)] = value_block

    for context in wcep_contexts :
        bb_id = context.source_block.split( "_b" )[ 1 ]
        context_id = context.source_context

        t0 = time.time()
        vblock = value_block_dict.get( (bb_id, context_id), None )

        if vblock is None:
            t1 = time.time()

            Time[ "val_instruction" ] += t1 - t0
            Time[ "count" ] += 1
            continue

        val_instructions = vblock.findall( f"a3:value_instruction/{XPATH_DFILOAD_VALUE_STEP}/..", NS )
        t1 = time.time()

        extract_tags_from_val_insts( value_instructions=val_instructions,
                                     context_id=context_id,
                                     data=data,
                                     count=context.count )
        t2 = time.time()

        Time["val_instruction"] += t1 - t0
        Time["extract_tag"] += t2 - t1
        Time["count"] += 1

    return data


def extract_dfi_value_analysis_from_xml( xml_file: Path ) -> A3_VALUE_ANALYSIS :
    """ Function used to extract all the data aiT can provide that would result in reducing some load set"""

    xml_root = ET.parse( xml_file ).getroot()

    data: A3_VALUE_ANALYSIS = { }
    xpath_value_block = f"{XPATH_VALUE_BLOCK}/a3:value_instruction/{XPATH_DFILOAD_VALUE_STEP}/../.."
    value_blocks = xml_root.findall( xpath_value_block, NS )

    for block in value_blocks :
        context_id = block.get( "context_id" )
        xpath_value_inst = f"./a3:value_instruction/{XPATH_DFILOAD_VALUE_STEP}/.."
        value_instructions = block.findall( xpath_value_inst, NS )

        extract_tags_from_val_insts( value_instructions=value_instructions, context_id=context_id, data=data )

    return data


def extract_wcep_context_data_from_xml( xml_file: Path ) -> A3_VALUE_ANALYSIS :
    wcep_contexts: List[ WCEPContext ] = extract_wcep_context_from_xml( xml_file )
    data: A3_VALUE_ANALYSIS = extract_tag_from_wcep_context( xml_file, wcep_contexts )

    return data
