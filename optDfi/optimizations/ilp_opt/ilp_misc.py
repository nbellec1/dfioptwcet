# coding=utf-8
from typing import Tuple, Dict, Iterable, Union, List

from pulp import LpProblem, LpBinary, LpVariable, lpSum, LpInteger, LpAffineExpression


def bin_or( pb: LpProblem, bs: List[LpVariable], name: str ) -> LpVariable:
    or_var = LpVariable( f"bin_or_{name}", cat=LpBinary )
    for i, bvar in enumerate(bs):
        pb += ( or_var >= bvar, f"lowB_bin_or_{name}_{i}" )

    pb += ( or_var <= lpSum( bs ) , f"upB_bin_or_{name}" )

    return or_var

def ilp_gt( pb: LpProblem, v1: Union[ LpVariable, LpAffineExpression ], v2: Union[ LpVariable, int ], name: str,
            big_m: int = 10_000 ) -> LpVariable :
    gt = LpVariable( f"gt_{name}", cat=LpBinary )
    pb += (gt >= (v1 - v2) / big_m, f"lower_bound_gt_{name}")
    pb += (gt <= 1 - (v2 - v1 + 1) / big_m, f"upper_bound_gt_{name}")

    return gt


def ilp_eq( pb: LpProblem, v1: LpVariable, v2: Union[ LpVariable, int ], name: str,
            big_m: int = 10_000 ) -> LpVariable :
    eq = LpVariable( f"eq_{name}", cat=LpBinary )
    pb += (eq == 1 - ilp_gt( pb, v1, v2, f"eq_v1_v2_{name}", big_m ) - ilp_gt( pb, v2, v1, f"eq_v2_v1_{name}", big_m ),
           f"bounds_eq_{name}")

    return eq


def max_ilp( pb: LpProblem, vars_to_max: Iterable[ LpVariable ], name: str, big_m: int = 10_000 ) -> LpVariable :
    v_max = LpVariable( f"max_{name}", cat=LpInteger, lowBound=max( map( lambda v : v.lowBound, vars_to_max ) ),
                        upBound=max( map( lambda v : v.upBound, vars_to_max ) ) )

    m_vars = LpVariable.dicts( f"m_{name}_", vars_to_max, cat=LpBinary )
    pb += (lpSum( m_vars.values() ) == 1, f"m_{name}_unicity")

    for i, var in enumerate( vars_to_max ) :
        pb += (v_max >= var, f"lower_bound_max_{name}_{i}")
        pb += (v_max <= var + big_m * (1 - m_vars[ var ]), f"upper_bound_max_{name}_{i}")

    return v_max


def min_max_bound_expr( expr: Union[ LpAffineExpression, LpVariable ] ) \
        -> Tuple[ Union[ int, None ], Union[ int, None ] ] :
    if type( expr ) == LpVariable :
        return expr.lowBound, expr.upBound

    upBound = 0
    lowBound = 0

    for var, val in expr.items() :
        if val > 0 :
            upBound = upBound + val * var.upBound if var.upBound is not None and upBound is not None else None
            lowBound = lowBound + val * var.lowBound if var.lowBound is not None and lowBound is not None else None
        elif val < 0 :
            upBound = upBound + val * var.lowBound if var.lowBound is not None and upBound is not None else None
            lowBound = lowBound + val * var.upBound if var.upBound is not None and lowBound is not None else None

    return lowBound, upBound


def prod_var_bin( pb: LpProblem, int_var: LpVariable, bin_var: LpVariable, name: str ) -> LpVariable :
    # /!\ Only works if int_var.lowBound >= 0
    assert int_var.lowBound >= 0
    assert int_var.upBound is not None

    v_prod = LpVariable( f"prod_{name}", cat=LpInteger, lowBound=min( 0, int_var.lowBound ), upBound=int_var.upBound )

    pb += (v_prod <= int_var.upBound * bin_var, f"prod_{name}_upBound_bin")
    pb += (v_prod <= int_var, f"prod_{name}_upBound_int")
    pb += (v_prod >= int_var - int_var.upBound * (1 - bin_var), f"prod_{name}_lowBound")

    return v_prod


def ilp_absdiff( pb: LpProblem, var1: LpVariable, var2: LpVariable, name: str, big_m: int = 10_000 ) \
        -> Tuple[ LpVariable, LpVariable, LpVariable, LpAffineExpression ] :
    a = ilp_gt( pb, var1, var2, f"{name}", big_m )

    # a(r1 - r2) + (1 - a)(r2 - r1) = r2 - r1 + 2*a*r1 - 2*a*r2
    a_r1 = prod_var_bin( pb, var1, a, f"{name}_a_{var1}" )
    a_r2 = prod_var_bin( pb, var2, a, f"{name}_a_{var2}" )

    return a, a_r1, a_r2, var2 - var1 + 2 * a_r1 - 2 * a_r2


def print_vars( variables: Union[ Dict[ str, LpVariable ], Iterable[ LpVariable ] ] ) :
    if type( variables ) == dict :
        for v in variables :
            print( f"{variables[ v ].name}: {variables[ v ].varValue}" )
    else :
        for v in variables :
            print( f"{v.name}: {v.varValue}" )
