#!/usr/bin/env python
# coding=utf-8

import logging
from pprint import pprint

import os
import pickle
from datetime import datetime
from pathlib import Path
from typing import Optional, Literal

from configurations.benchmarks import benchmarks
from optDfi.experiment.CSVResult import CSVResult
from optDfi.experiment.FileSystem import FileSystem

from optDfi.experiment.experiment import experiment_iteration_vs_witness, build_benchmark, experiment_lschain, TimingTy, \
    ResultTy, rtdfi_and_lschain
from optDfi.misc.utils import color_str, print_color, Color
from optDfi.optimizations.ilp_opt.ilp_opt import OptDirected
from optDfi.optimizations.ilp_opt.ilp_problem.problem import IlpIntervalProblem, IlpIntervalSolution
from optDfi.optimizations.optimization_misc import LLVM_TAG_REORDER, scan_llvm_bytecode_metadata
from optDfi.systemInteraction.makefileGen import genCommonBenchMakefile, genOptionMakefile


def print_tag( ilp_constructor, d ) :
    for t in d :
        print(
            f"{t} ({ilp_constructor.vertex_index_vars[ t ].varValue}) : {d[ t ].varValue}" )


def print_interval_order( problem, ilp_constructor ) :
    for load in problem.load_traversal() :
        loadid = load.loadid
        print( f"--- {loadid} ---" )
        print_tag( ilp_constructor, ilp_constructor.load_index_vars[ loadid ] )


def main_build() :
    timestamp = int( datetime.now().timestamp() )
    exp_name = f"build-bench"
    fs = FileSystem( Path( os.getcwd() ) / f"{timestamp}/{exp_name}/" )

    for bench in benchmarks :
        try :
            print( bench.tag )
            bench_fs = FileSystem( fs.base_dir, bench.tag )
            build_benchmark( bench, bench_fs )

        except Exception as e :
            logging.error( color_str( f"BENCHMARK EXCEPTION RAISED : {str( e )}", Color.RED ) )


def main_lschain() :
    lschain_additive_passes = [
        "sandboxElim", "wordChain", "chainCut"
    ]
    exp_name = f"lschains"
    fs = FileSystem( Path( os.getcwd() ) /
                     f"{exp_name}/" )
    result_file = Path( fs.base_dir / "result.csv" )

    result_csv_characteristics = [ "witnessWCET", "sotaWCET" ] + \
                                 [ f"lschain{opt}WCET" for opt in lschain_additive_passes ]
    result_csv = CSVResult(
        characteristics=result_csv_characteristics, file=result_file )

    result_csv.write_header()

    for bench in benchmarks :
        try :
            logging.warning( f"Start: {bench.tag}" )
            bench_fs = FileSystem( fs.base_dir, bench.tag )
            result_csv.add_bench( bench.tag )

            experiment_lschain( bench, bench_fs, result_csv=result_csv,
                                lschain_additive_passes=lschain_additive_passes,
                                compute_wcet=True, compute_comet=False )

            result_csv.write_bench_data( bench.tag )
        except Exception as e :
            logging.error( f"BENCHMARK EXCEPTION RAISED : {str( e )}" )

def main_test_makefile() :
    genCommonBenchMakefile( benchmarks )


def main_lschain_rtdfi():
    time_limit = 3 * 60 # CPLEX maximum time seconds
    lschain_additive_passes = [
        "sandboxElim", "wordChain", "chainCut"
    ]

    exp_name = f"rtdfi-lschains"
    fs = FileSystem( Path( os.getcwd() ) /
                     f"{exp_name}/" )
    result_file = Path( fs.base_dir / "result.csv" )

    result_csv_characteristics = [ "witnessWCET", "sotaWCET" ] + \
                                 [ f"lschain{opt}WCET" for opt in lschain_additive_passes ] +\
                                 [ "vaWCET", "rtdfiWCET" ]
    result_csv = CSVResult(
        characteristics=result_csv_characteristics, file=result_file )

    result_csv.write_header()

    for bench in benchmarks :
        try :
            print_color( f"Start : {str(bench.tag)}", Color.RED )
            bench_fs = FileSystem( fs.base_dir, bench.tag )
            result_csv.add_bench( bench.tag )
            # timing_csv.add_bench( bench.tag )

            rtdfi_and_lschain(bench, bench_fs, result_csv, time_limit, lschain_additive_passes )

            result_csv.write_bench_data( bench.tag )
        except Exception as e :
            logging.error( f"BENCHMARK EXCEPTION RAISED : {str( e )}" )

    # result_csv.write_all_data()

def main_rtdfi() :
    time_limit = 3 * 60
    slack = 0  # Final formula is 1 + (slack/100)
    iteration = 4
    timestamp = int( datetime.now().timestamp() )
    exp_name = f"{time_limit}s-{slack}"
    fs = FileSystem( Path( os.getcwd() ) / "experiments" /
                     f"{timestamp}/{exp_name}/" )
    result_file = Path( fs.base_dir / "result.csv" )
    timing_file = Path( fs.base_dir / "timing.csv" )

    result_csv_characteristics = [ "witness_wcet", "sota_wcet", "value_analysis_wcet" ] + \
                                 [ f"ilp_{i}_wcet" for i in range( iteration ) ] + \
                                 [ "it_num" ]
    result_csv = CSVResult(
        characteristics=result_csv_characteristics, file=result_file )

    timing_csv_characteristics = [ "witness_build", "witness_wcet", "sota_build", "sota_wcet", "pre_value_build",
                                   "pre_value_wcet", "value_analysis", "value_analysis_build", "value_analysis_wcet" ] + \
                                 [ f"ilp_{i}_{tag}" for i in range( iteration ) for tag in
                                   [ "analysis", "build", "wcet" ] ]
    timing_csv = CSVResult(
        characteristics=timing_csv_characteristics, file=timing_file )

    for bench in benchmarks :
        try :
            bench_fs = FileSystem( fs.base_dir, bench.tag )
            result_csv.add_bench( bench.tag )
            timing_csv.add_bench( bench.tag )

            experiment_iteration_vs_witness(
                bench, bench_fs, result_csv, timing_csv, time_limit, slack, iteration )

        except Exception as e :
            logging.error( color_str( f"BENCHMARK EXCEPTION RAISED : {str( e )}", Color.RED ) )

    timing_csv.write_all_data()
    result_csv.write_all_data()


if __name__ == '__main__' :
    # main_lschain_rtdfi()
    # logging.root.setLevel(logging.INFO)
    # main()
    # main_test_makefile()
    main_lschain()
