#.PHONY: init init-dev test test-all test-fast test-slow test-time coverage mutate \
#		clean clean-mutate clean-coverage clean-test test-debug
#
#init-dev:
#	pipenv install -d
#
#init:
#	pipenv install
#	pipenv shell
#
#test: test-all
#
#test-debug:
#	pytest --maxfail=1 tests/
#
#test-all:
#	pytest -n 4 tests/
#
#test-fast:
#	pytest -n 4 -m "not slow" tests/
#
#test-slow:
#	pytest -n 4 -m "slow" tests/
#
#test-time:
#	@pytest -n 4 --durations=0 tests/ > .pytest-durations
#
#coverage:
#	pytest -n 4 --cov=optDfi --cov-branch --cov-report term-missing:skip-covered \
#		   --cov-report html:cov_html tests/
#
#mutate:
#	@-mutmut --paths-to-mutate=optDfi run || echo ""
#	@mutmut results > .mutate-results
#
#clean: clean-mutate clean-test clean-coverage
#
#clean-mutate:
#	@rm -f .mutate-results .mutmut-cache
#
#clean-test:
#	@rm -rf .pytest_cache .pytest-durations
#
#clean-coverage:
#	@rm -rf cov_html .coverage

