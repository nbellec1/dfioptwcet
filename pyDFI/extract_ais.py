# coding=utf-8

""" Use to extract ais result_file from the loop printer """

import re
import sys

loop_stack = []
loop_data = {}

class Loop( object ):
    def __init__(self, fname, lid):
        self.fname = fname
        self.lid = lid
        self.it = 0

    def __repr__( self ):
        return "<Loop {fname}, {lid}, {it}>".format(
            fname=self.fname,
            lid=self.lid,
            it=self.it
        )

class Function( object ):
    def __init__(self, fname):
        self.fname = fname

    def __repr__(self):
        return "<Function {fname}>".format(
            fname=self.fname
        )

def entry_loop( fname, lid, index ):
    global loop_stack

    if len( loop_stack ) == 0:
        raise Exception("Loop entry without function entry L."+str(index) )

    last = loop_stack.pop()
    loop = None
    if type( last ) == Function:
        loop_stack.append( last )
        loop = Loop(fname, lid)
    elif type( last ) == Loop and last.fname == fname and last.lid == lid:
        loop = last
    else:
        loop_stack.append( last )
        loop = Loop( fname, lid )

    loop.it += 1
    loop_stack.append( loop )

def exit_loop( fname, lid, index ):
    global loop_stack, loop_data

    if len( loop_stack ) == 0:
        raise Exception( "Loop exit with empty stack L."+str(index) )

    last = loop_stack.pop()

    if type( last ) != Loop or last.fname != fname or last.lid != lid:
        # raise Exception( "Exit loop that is not on top of stack L."+str(index) )
        loop_stack.append( last )
        return

    data = loop_data.get( tuple([last.fname, last.lid]) )

    if data is None:
        loop_data[tuple([last.fname, last.lid])] = [ last.it ]
    else:
        loop_data[tuple([last.fname, last.lid])].append( last.it )

def entry_func( fname, index ):
    global loop_stack

    func = Function( fname )
    loop_stack.append( func )

def exit_func( fname, index ):
    global loop_stack

    if len( loop_stack ) == 0:
        raise Exception( "Exit function with empty stack L."+str(index) )

    last = loop_stack.pop()

    if type( last ) != Function or last.fname != fname:
        raise Exception( "Exit function not on top of stack L."+str(index) )


entry_function_re = re.compile( "Entry function : ([0-9a-zA-Z_]*)" )
exit_function_re = re.compile( "Exit function : ([0-9a-zA-Z_]*)" )
entry_loop_re = re.compile( "Entry loop : ([0-9a-zA-Z_]*) ! (0x[0-9a-f]*)" )
exit_loop_re = re.compile( "Exit loop : ([0-9a-zA-Z_]*) ! (0x[0-9a-f]*)" )

if len( sys.argv ) < 2 :
    raise Exception( "Must provide a result_file as first parameter" )

filename = sys.argv[ 1 ]

with open( filename, "r" ) as file:
    for i, entry in enumerate( file ):
        m = entry_function_re.search( entry )
        if m is not None:
            entry_func( fname=m.group( 1 ), index=i )
            continue
        m = exit_function_re.search( entry )
        if m is not None:
            exit_func( fname=m.group( 1 ), index=i )
            continue
        m = entry_loop_re.search( entry )
        if m is not None:
            entry_loop( fname=m.group( 1 ), lid=int( m.group( 2 ), base=16 ), index=i )
            continue
        m = exit_loop_re.search( entry )
        if m is not None :
            exit_loop( fname=m.group( 1 ), lid=int( m.group( 2 ), base=16 ), index=i )
            continue
        # Skip empty lines + KILL line
        if len( entry ) < 6:
            continue
        # default case
        print( entry )

# print( "--- LOOP DATA ---" )
for p, d in loop_data.items():
    s1 = "loop \"{fct}.L{lid:d}\" ".format(
        fct=p[0],
        lid=p[1] + 1,
    )
    s2 = "bound: {min:d} .. {max:d};".format(
        min=min( d ),
        max=max( d )
    )
    print( s1 + "{ " + s2 +" }" )
