# coding=utf-8
from copy import deepcopy
from typing import List, Dict, Hashable, TypeVar, Iterable

from optDfi.optimizations.ilp_opt.ilp_problem.context import ExactContext, PartialContext, UnknownContext, LoadContexts
from optDfi.optimizations.ilp_opt.ilp_problem.equivalence_map import EqClassMap

T = TypeVar( 'T', bound=Hashable )
EC = ExactContext
PC = PartialContext
UC = UnknownContext

# Utility function to test the correct construction of LoadContexts
def count_dict_from_list( iterable: Iterable[ T ] ) -> Dict[ T, int ] :
    d1: Dict[ T, int ] = dict()

    for c in iterable :
        if c not in d1 :
            d1[ c ] = 0
        d1[ c ] += 1

    return d1


def eq_context_lists( l1: List[ T ], l2: List[ T ] ) -> bool :
    if len( l1 ) != len( l2 ) :
        return False

    d1 = count_dict_from_list( l1 )
    d2 = count_dict_from_list( l2 )

    return d1 == d2

def eq_load_context( lc1: LoadContexts, lc2: LoadContexts ) -> bool :
    return lc1.loadid == lc2.loadid and \
           lc1.tags == lc2.tags and \
           eq_context_lists( lc1.exacts, lc2.exacts ) and \
           eq_context_lists( lc1.partial, lc2.partial ) and \
           eq_context_lists( lc1.unknown, lc2.unknown )

def assert_eq_context_list( l1: List[ T ], l2: List[ T ] ) :
    d1 = count_dict_from_list( l1 )
    d2 = count_dict_from_list( l2 )

    assert d1 == d2
    assert len( l1 ) == len( l2 )

def assert_eq_load_context( lc1: LoadContexts, lc2: LoadContexts ):
    assert lc1.loadid == lc2.loadid
    assert lc1.tags == lc2.tags
    assert_eq_context_list( lc1.exacts, lc2.exacts )
    assert_eq_context_list( lc1.partial, lc2.partial )
    assert_eq_context_list( lc1.unknown, lc2.unknown )


def test_load_context_fusion() :
    # No fusion
    lc1 = LoadContexts( loadid="lc1",
                        exacts=[ EC( 'A', 10 ),
                                 EC( 'B', 20 ) ],
                        partial=[
                            PC( frozenset(('A', 'B')), 10 ),
                            PC( frozenset(('A', 'C')), 15 ),
                        ],
                        unknown=[
                            UC( 20 )
                        ],
                        tags={ 'A', 'B', 'C' } )
    lc2 = deepcopy( lc1 ).fusion_optimization()

    assert eq_load_context( lc1, lc2 )

    # Exact only
    lc1 = LoadContexts( loadid="lc1",
                        exacts=[ EC( 'A', 10 ),
                                 EC( 'A', 20 ) ],
                        tags={ 'A' } )
    lc2 = LoadContexts( loadid="lc1",
                        exacts=[ EC( 'A', 30 ) ],
                        tags={ 'A' } )
    lc_eq = deepcopy( lc1 ).fusion_optimization()

    assert eq_load_context( lc2, lc_eq )

    # Unknown only
    lc1 = LoadContexts( loadid="lc1",
                        unknown=[ UC( 10 ),
                                  UC( 15 ) ],
                        tags={ 'A' } )
    lc2 = LoadContexts( loadid="lc1",
                        unknown=[ UC( 25 ) ],
                        tags={ 'A' } )
    lc_eq = deepcopy( lc1 ).fusion_optimization()

    assert eq_load_context( lc2, lc_eq )

    # Partial only
    lc1 = LoadContexts( loadid="lc1",
                        partial=[ PC( frozenset(('A', 'B')), 10 ),
                                  PC( frozenset(('A', 'B')), 15 ) ],
                        tags={ 'A', 'B', 'C' } )
    lc2 = LoadContexts( loadid="lc1",
                        partial=[ PC( frozenset(('A', 'B')), 25 ) ],
                        tags={ 'A', 'B', 'C' } )
    lc_eq = deepcopy( lc1 ).fusion_optimization()

    assert eq_load_context( lc2, lc_eq )

    # Full test
    lc1 = LoadContexts( loadid="lc1",
                        exacts=[
                            EC( 'A', 10 ),
                            EC( 'B', 20 ),
                            EC( 'A', 5 ),
                            EC( 'C', 25 ),
                            EC( 'C', 10 )
                        ],
                        partial=[
                            PC( frozenset( ('A', 'B') ), 10 ),
                            PC( frozenset( ('A', 'C') ), 15 ),
                            PC( frozenset( ('A', 'B') ), 15 ),
                            PC( frozenset( ('B', 'C') ), 20 ),
                            PC( frozenset( ('A', 'C') ), 5 ),
                        ],
                        unknown=[
                            UC( 20 ),
                            UC( 10 ),
                            UC( 70 ),
                        ],
                        tags={ 'A', 'B', 'C' } )
    lc2 = LoadContexts( loadid="lc1",
                        exacts=[
                            EC( 'A', 15 ),
                            EC( 'B', 20 ),
                            EC( 'C', 35 ),
                        ],
                        partial=[
                            PC( frozenset( ('A', 'B') ), 25 ),
                            PC( frozenset( ('A', 'C') ), 20 ),
                            PC( frozenset( ('B', 'C') ), 20 ),
                        ],
                        unknown=[
                            UC( 100 )
                        ],
                        tags={ 'A', 'B', 'C' } )
    lc_eq = deepcopy( lc1 ).fusion_optimization()

    assert eq_load_context( lc2, lc_eq )

def test_load_context_remove_null_count() :
    lc1 = LoadContexts(
        loadid="lc1",
        exacts=[
            EC( 'A', 0 ),
            EC( 'B', 10),
            EC( 'C', 0 )
        ],
        partial=[
            PC( frozenset( ('A', 'B') ), 10 ),
            PC( frozenset( ('A', 'C') ), 15 ),
            PC( frozenset( ('A', 'B') ), 0 ),
            PC( frozenset( ('B', 'C') ), 20 ),
            PC( frozenset( ('A', 'C') ), 0 ),
        ],
        unknown=[
            UC( 5 ),
            UC( 0 ),
            UC( 10 ),
        ],
        tags={ 'A', 'B', 'C', 'D' }
    )

    lc2 = LoadContexts( loadid="lc1",
                        exacts=[
                            EC( 'B', 10 ),
                        ],
                        partial=[
                            PC( frozenset( ('A', 'B') ), 10 ),
                            PC( frozenset( ('A', 'C') ), 15 ),
                            PC( frozenset( ('B', 'C') ), 20 ),
                        ],
                        unknown=[
                            UC( 5 ),
                            UC( 10 ),
                        ],
                        tags={ 'A', 'B', 'C', 'D' } )
    lc_eq = deepcopy( lc1 ).remove_null_count_contexts()

    assert_eq_load_context( lc2, lc_eq )

def test_load_context_apply_eqclass_tags() :
    lc1 = LoadContexts(
        loadid="lc1",
        exacts=[
            EC( 'A', 15 ),
            EC( 'B', 10),
            EC( 'C', 40 )
        ],
        partial=[
            PC( frozenset( ('A', 'B') ), 10 ),
            PC( frozenset( ('A', 'C') ), 15 ),
            PC( frozenset( ('A', 'B') ), 50 ),
            PC( frozenset( ('B', 'C') ), 20 ),
            PC( frozenset( ('A', 'C') ), 10 ),
        ],
        unknown=[
            UC( 5 ),
            UC( 8 ),
            UC( 10 ),
        ],
        tags={ 'A', 'B', 'C', 'D' }
    )

    eqclass = EqClassMap( {'A', 'B', 'C', 'D'} )
    eqclass.fusion_element_class( 'A', 'C' )

    lc2 = LoadContexts(
        loadid="lc1",
        exacts=[
            EC( 'A', 15 ),
            EC( 'B', 10 ),
            EC( 'A', 40 )
        ],
        partial=[
            PC( frozenset( ('A', 'B') ), 10 ),
            PC( frozenset( ('A', 'A') ), 15 ),
            PC( frozenset( ('A', 'B') ), 50 ),
            PC( frozenset( ('B', 'A') ), 20 ),
            PC( frozenset( ('A', 'A') ), 10 ),
        ],
        unknown=[
            UC( 5 ),
            UC( 8 ),
            UC( 10 ),
        ],
        tags={ 'A', 'B', 'A', 'D' }
    )
    lc_eq = lc1.apply_eqclass_tags( eqclass )

    assert_eq_load_context( lc2, lc_eq )


