# coding=utf-8

import pytest
import optDfi.interface.objdump as objdump
from optDfi.interface.misc import search_in_instructions, get_index_inst_between
from tests.data.test_interface.data_objdump import construct_lui_inst_list


class TestAiTInterface :
    """
    Tests for the objdump interface
    """

    def test_search( self ) :
        insts = construct_lui_inst_list( 40, add_start=0x80000000 )
        assert insts[ search_in_instructions( insts, 0x80000014 ) ].address == 0x80000014
        assert insts[ search_in_instructions( insts, 0x80000004 ) ].address == 0x80000004
        assert insts[ search_in_instructions( insts, 0x80000000 ) ].address == 0x80000000
        assert insts[ search_in_instructions( insts, 0x80000000 + 4*39 ) ].address == 0x80000000 + 4*39
