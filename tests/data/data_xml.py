# coding=utf-8
import xml.etree.ElementTree as ET
import networkx as nx

ns = { 'a3' : 'http://www.absint.com/a3report' }
path_wcet_analysis_task = "a3:wcet_analysis_task"
path_cfg_analysis = path_wcet_analysis_task + "/a3:cfg_value_analysis"
path_routines = path_cfg_analysis + "/a3:routines/a3:routine"
path_wcet_path = path_wcet_analysis_task + "/a3:wcet_analysis/a3:wcet_path"


def simple_extract_cfg_from_routine( pathname, routine_name ) :
    """
    Utility function used to extract a first cfg that is then refactored into the real
    cfg by hand

    :param pathname: path to the result_file
    :param routine_name: name of the routine in the xml
    :return: None
    """
    tree = ET.parse( pathname )
    routine_node = tree.find( path_routines + "[@name={name}]".format( name=repr( routine_name ) ),
                              ns )

    for block_node in routine_node.findall( "a3:block", ns ) :
        for successor_block in block_node.findall( "a3:successor", ns ) :
            print(
                "({block_id}, {succ_id}),".format(
                    block_id=repr( block_node.attrib[ "id" ] ),
                    succ_id=repr( successor_block.text ) )
            )


def simple_extract_icfg( pathname ) :
    """
    Utility function used to extract a first icfg that is then refactored into the real
    icfg by hand

    :param pathname: path to the result_file
    :return: None
    """
    tree = ET.parse( pathname )
    call_graph_node = tree.find( ".//a3:call_graph", ns )

    for call_node in call_graph_node.findall( "./a3:call", ns ) :
        for called in call_node.findall( "./a3:call", ns ) :
            called_block_list = list( map( lambda cb : cb.attrib[ "block" ],
                                           called.findall( "a3:called_from", ns ) ) )
            print( "({call_node_id}, {called_node_id}, {{ callIds: {callIds} }}),".format(
                call_node_id=repr( call_node.attrib[ "routine" ] ),
                called_node_id=repr( called.attrib[ "routine" ] ),
                callIds=repr( called_block_list )
            ) )


# Data about simple.xml

simple_icfg = nx.DiGraph()
simple_icfg.add_node( "d1_r2" )

simple_kernel_cfg = nx.DiGraph()
simple_kernel_cfg.add_edges_from( [
    ('d1_b3', 'd1_b5'),
    ('d1_b5', 'd1_b4'),
] )

simple_icfg.nodes[ "d1_r2" ][ "cfg" ] = simple_kernel_cfg

simple_wcet_path = nx.DiGraph()

simple_wcet_path.add_node( "d1_r2:d1_r2_c0:d1_b3", start=True, start_routine=True )
simple_wcet_path.add_node( "d1_r2:d1_r2_c0:d1_b4", end=True, end_routine=True )

simple_wcet_path.add_edges_from( [
    ("d1_r2:d1_r2_c0:d1_b3", "d1_r2:d1_r2_c0:d1_b5", dict( count=1 )),
    ("d1_r2:d1_r2_c0:d1_b5", "d1_r2:d1_r2_c0:d1_b4", dict( count=1, cycles=33 )),
] )

# Data about branch.xml

branch_icfg = nx.DiGraph()
branch_icfg.add_node( "d1_r2" )

branch_kernel_cfg = nx.DiGraph()
branch_kernel_cfg.add_edges_from( [
    ('d1_b3', 'd1_b5'),
    ('d1_b5', 'd1_b23'),
    ('d1_b5', 'd1_b18'),
    ('d1_b18', 'd1_b27'),
    ('d1_b23', 'd1_b27'),
    ('d1_b27', 'd1_b4'),
] )

branch_icfg.nodes[ "d1_r2" ][ "cfg" ] = branch_kernel_cfg

branch_wcet_path = nx.DiGraph()

branch_wcet_path.add_node( "d1_r2:d1_r2_c0:d1_b3", start=True, start_routine=True )
branch_wcet_path.add_node( "d1_r2:d1_r2_c0:d1_b4", end=True, end_routine=True )

branch_wcet_path.add_edges_from( [
    ("d1_r2:d1_r2_c0:d1_b3", "d1_r2:d1_r2_c0:d1_b5", dict( count=1 )),
    ("d1_r2:d1_r2_c0:d1_b5", "d1_r2:d1_r2_c0:d1_b18", dict( count=1, cycles=16 )),
    ("d1_r2:d1_r2_c0:d1_b18", "d1_r2:d1_r2_c0:d1_b27", dict( count=1, cycles=4 )),
    ("d1_r2:d1_r2_c0:d1_b27", "d1_r2:d1_r2_c0:d1_b4", dict( count=1, cycles=11 )),
] )

# Data about loop.xml

loop_icfg = nx.DiGraph()
loop_icfg.add_node( "d1_r2" )

loop_kernel_cfg = nx.DiGraph()
loop_kernel_cfg.add_edges_from( [
    # kernel
    ('d1_b3', 'd1_b5'),
    ('d1_b5', 'd1_b50'),
    ('d1_b29', 'd1_b4'),
    ('d1_b50', 'd1_b51'),
    ('d1_b50', 'd1_b41'),
    ('d1_b51', 'd1_b29'),
    # kernel.L1
    ('d1_b41', 'd1_b23'),
    ('d1_b42', 'd1_b44'),
    ('d1_b42', 'd1_b51'),
    ('d1_b14', 'd1_b43'),
    ('d1_b23', 'd1_b14'),
    ('d1_b23', 'd1_b42'),
    ('d1_b43', 'd1_b44'),
    ('d1_b43', 'd1_b41'),
    ('d1_b44', 'd1_b42'),
] )

loop_icfg.nodes[ "d1_r2" ][ "cfg" ] = loop_kernel_cfg

loop_wcet_path = nx.DiGraph()

loop_wcet_path.add_node( "d1_r2:d1_r2_c0:d1_b3", start=True, start_routine=True )
loop_wcet_path.add_node( "d1_r40:d1_r40_c0:d1_b41", start_routine=True )
loop_wcet_path.add_node( "d1_r40:d1_r40_c1:d1_b41", start_routine=True )

loop_wcet_path.add_node( "d1_r2:d1_r2_c0:d1_b4", end=True, end_routine=True )
loop_wcet_path.add_node( "d1_r40:d1_r40_c0:d1_b42", end_routine=True )
loop_wcet_path.add_node( "d1_r40:d1_r40_c1:d1_b42", end_routine=True )

loop_wcet_path.add_edges_from( [
    ("d1_r2:d1_r2_c0:d1_b3", "d1_r2:d1_r2_c0:d1_b5", dict( count=1 )),
    ("d1_r2:d1_r2_c0:d1_b5", "d1_r2:d1_r2_c0:d1_b50", dict( count=1, cycles=12 )),
    # "Call" to the loop c0
    ("d1_r2:d1_r2_c0:d1_b50", "d1_r40:d1_r40_c0:d1_b41", dict( count=1 )),
    ("d1_r40:d1_r40_c0:d1_b41", "d1_r40:d1_r40_c0:d1_b23", dict( count=1 )),
    ("d1_r40:d1_r40_c0:d1_b23", "d1_r40:d1_r40_c0:d1_b14", dict( count=1, cycles=6 )),
    ("d1_r40:d1_r40_c0:d1_b14", "d1_r40:d1_r40_c0:d1_b43", dict( count=1, cycles=13 )),
    # "Call" to the loop l1
    ("d1_r40:d1_r40_c0:d1_b43", "d1_r40:d1_r40_c1:d1_b41", dict( count=1 )),
    ("d1_r40:d1_r40_c1:d1_b41", "d1_r40:d1_r40_c1:d1_b23", dict( count=10 )),
    ("d1_r40:d1_r40_c1:d1_b23", "d1_r40:d1_r40_c1:d1_b14", dict( count=9, cycles=45 )),
    ("d1_r40:d1_r40_c1:d1_b23", "d1_r40:d1_r40_c1:d1_b42", dict( count=1, cycles=5 )),
    ("d1_r40:d1_r40_c1:d1_b14", "d1_r40:d1_r40_c1:d1_b43", dict( count=9, cycles=117 )),
    # "Intra-call" to loop l1
    ("d1_r40:d1_r40_c1:d1_b43", "d1_r40:d1_r40_c1:d1_b41", dict( count=9 )),
    # End "intra-call" to loop l1
    ("d1_r40:d1_r40_c1:d1_b42", "d1_r40:d1_r40_c1:d1_b44", dict( count=9 )),
    ("d1_r40:d1_r40_c1:d1_b44", "d1_r40:d1_r40_c1:d1_b42", dict( count=9 )),
    # End loop l1
    ("d1_r40:d1_r40_c1:d1_b42", "d1_r40:d1_r40_c0:d1_b44", dict( count=1 )),
    ("d1_r40:d1_r40_c0:d1_b44", "d1_r40:d1_r40_c0:d1_b42", dict( count=1 )),
    # End loop c0
    ("d1_r40:d1_r40_c0:d1_b42", "d1_r2:d1_r2_c0:d1_b51", dict( count=1 )),
    ("d1_r2:d1_r2_c0:d1_b51", "d1_r2:d1_r2_c0:d1_b29", dict( count=1 )),
    ("d1_r2:d1_r2_c0:d1_b29", "d1_r2:d1_r2_c0:d1_b4", dict( count=1, cycles=9 )),
] )

# Data about multi_loops.xml

multi_loops_icfg = nx.DiGraph()
multi_loops_icfg.add_node( "d1_r2" )

multi_loops_kernel_cfg = nx.DiGraph()
multi_loops_kernel_cfg.add_edges_from( [
    # kernel
    ('d1_b3', 'd1_b5'),
    ('d1_b5', 'd1_b83'),
    ('d1_b46', 'd1_b4'),
    ('d1_b83', 'd1_b84'),
    ('d1_b83', 'd1_b74'),
    ('d1_b84', 'd1_b46'),
    # kernel.L1
    ('d1_b58', 'd1_b29'),
    ('d1_b59', 'd1_b61'),
    ('d1_b59', 'd1_b68'),
    ('d1_b18', 'd1_b60'),
    ('d1_b29', 'd1_b18'),
    ('d1_b29', 'd1_b59'),
    ('d1_b60', 'd1_b61'),
    ('d1_b60', 'd1_b58'),
    ('d1_b61', 'd1_b59'),
    # kernel.L2
    ('d1_b74', 'd1_b40'),
    ('d1_b75', 'd1_b77'),
    ('d1_b75', 'd1_b84'),
    ('d1_b14', 'd1_b67'),
    ('d1_b35', 'd1_b76'),
    ('d1_b40', 'd1_b14'),
    ('d1_b40', 'd1_b75'),
    ('d1_b67', 'd1_b68'),
    ('d1_b67', 'd1_b58'),
    ('d1_b68', 'd1_b35'),
    ('d1_b76', 'd1_b77'),
    ('d1_b76', 'd1_b74'),
    ('d1_b77', 'd1_b75'),
] )

multi_loops_icfg.nodes[ "d1_r2" ][ "cfg" ] = multi_loops_kernel_cfg

multi_loops_wcet_path = nx.DiGraph()

multi_loops_wcet_path.add_node( "d1_r2:d1_r2_c0:d1_b3", start=True, start_routine=True )
multi_loops_wcet_path.add_node( "d1_r57:d1_r57_c0:d1_b58", start_routine=True )
multi_loops_wcet_path.add_node( "d1_r57:d1_r57_c1:d1_b58", start_routine=True )
multi_loops_wcet_path.add_node( "d1_r57:d1_r57_c2:d1_b58", start_routine=True )
multi_loops_wcet_path.add_node( "d1_r57:d1_r57_c3:d1_b58", start_routine=True )
multi_loops_wcet_path.add_node( "d1_r73:d1_r73_c0:d1_b74", start_routine=True )
multi_loops_wcet_path.add_node( "d1_r73:d1_r73_c1:d1_b74", start_routine=True )

multi_loops_wcet_path.add_node( "d1_r2:d1_r2_c0:d1_b4", end=True, end_routine=True )
multi_loops_wcet_path.add_node( "d1_r57:d1_r57_c0:d1_b59", end_routine=True )
multi_loops_wcet_path.add_node( "d1_r57:d1_r57_c1:d1_b59", end_routine=True )
multi_loops_wcet_path.add_node( "d1_r57:d1_r57_c2:d1_b59", end_routine=True )
multi_loops_wcet_path.add_node( "d1_r57:d1_r57_c3:d1_b59", end_routine=True )
multi_loops_wcet_path.add_node( "d1_r73:d1_r73_c0:d1_b75", end_routine=True )
multi_loops_wcet_path.add_node( "d1_r73:d1_r73_c1:d1_b75", end_routine=True )


multi_loops_wcet_path.add_edges_from( [
    ("d1_r2:d1_r2_c0:d1_b3", "d1_r2:d1_r2_c0:d1_b5", dict( count=1 )),
    ("d1_r2:d1_r2_c0:d1_b5", "d1_r2:d1_r2_c0:d1_b83", dict( count=1, cycles=12 )),
    # "Call" to L1 c0
    ("d1_r2:d1_r2_c0:d1_b83", "d1_r73:d1_r73_c0:d1_b74", dict( count=1 )),
    ("d1_r73:d1_r73_c0:d1_b74", "d1_r73:d1_r73_c0:d1_b40", dict( count=1 )),
    ("d1_r73:d1_r73_c0:d1_b40", "d1_r73:d1_r73_c0:d1_b14", dict( count=1, cycles=6 )),
    ("d1_r73:d1_r73_c0:d1_b14", "d1_r73:d1_r73_c0:d1_b67", dict( count=1, cycles=5 )),
    # Call to L2 c0
    ("d1_r73:d1_r73_c0:d1_b67", "d1_r57:d1_r57_c0:d1_b58", dict( count=1 )),
    ("d1_r57:d1_r57_c0:d1_b58", "d1_r57:d1_r57_c0:d1_b29", dict( count=1 )),
    ("d1_r57:d1_r57_c0:d1_b29", "d1_r57:d1_r57_c0:d1_b18", dict( count=1, cycles=6 )),
    ("d1_r57:d1_r57_c0:d1_b18", "d1_r57:d1_r57_c0:d1_b60", dict( count=1, cycles=16 )),
    # Call to L2 l1
    ("d1_r57:d1_r57_c0:d1_b60", "d1_r57:d1_r57_c1:d1_b58", dict( count=1 )),
    ("d1_r57:d1_r57_c1:d1_b58", "d1_r57:d1_r57_c1:d1_b29", dict( count=10 )),
    ("d1_r57:d1_r57_c1:d1_b29", "d1_r57:d1_r57_c1:d1_b18", dict( count=9, cycles=45 )),
    ("d1_r57:d1_r57_c1:d1_b29", "d1_r57:d1_r57_c1:d1_b59", dict( count=1, cycles=5 )),
    ("d1_r57:d1_r57_c1:d1_b18", "d1_r57:d1_r57_c1:d1_b60", dict( count=9, cycles=144 )),
    # Intra-call to L2 l1
    ("d1_r57:d1_r57_c1:d1_b60", "d1_r57:d1_r57_c1:d1_b58", dict( count=9 )),
    # Intra-return from L2 l1
    ("d1_r57:d1_r57_c1:d1_b59", "d1_r57:d1_r57_c1:d1_b61", dict( count=9 )),
    ("d1_r57:d1_r57_c1:d1_b61", "d1_r57:d1_r57_c1:d1_b59", dict( count=9 )),
    # End to L2 l1
    ("d1_r57:d1_r57_c1:d1_b59", "d1_r57:d1_r57_c0:d1_b61", dict( count=1 )),
    ("d1_r57:d1_r57_c0:d1_b61", "d1_r57:d1_r57_c0:d1_b59", dict( count=1 )),
    # Return from L2 c0
    ("d1_r57:d1_r57_c0:d1_b59", "d1_r73:d1_r73_c0:d1_b68", dict( count=1 )),
    ("d1_r73:d1_r73_c0:d1_b68", "d1_r73:d1_r73_c0:d1_b35", dict( count=1 )),
    ("d1_r73:d1_r73_c0:d1_b35", "d1_r73:d1_r73_c0:d1_b76", dict( count=1, cycles=4 )),
    # Call to L1 l1
    ("d1_r73:d1_r73_c0:d1_b76", "d1_r73:d1_r73_c1:d1_b74", dict( count=1 )),
    ("d1_r73:d1_r73_c1:d1_b74", "d1_r73:d1_r73_c1:d1_b40", dict( count=10 )),
    ("d1_r73:d1_r73_c1:d1_b40", "d1_r73:d1_r73_c1:d1_b14", dict( count=9, cycles=45 )),
    ("d1_r73:d1_r73_c1:d1_b40", "d1_r73:d1_r73_c1:d1_b75", dict( count=1, cycles=5 )),
    ("d1_r73:d1_r73_c1:d1_b14", "d1_r73:d1_r73_c1:d1_b67", dict( count=9, cycles=45 )),
    # Call to L2 l2
    ("d1_r73:d1_r73_c1:d1_b67", "d1_r57:d1_r57_c2:d1_b58", dict( count=9 )),
    ("d1_r57:d1_r57_c2:d1_b58", "d1_r57:d1_r57_c2:d1_b29", dict( count=9 )),
    ("d1_r57:d1_r57_c2:d1_b29", "d1_r57:d1_r57_c2:d1_b18", dict( count=9, cycles=54 )),
    ("d1_r57:d1_r57_c2:d1_b18", "d1_r57:d1_r57_c2:d1_b60", dict( count=9, cycles=144 )),
    # Call to L2 c3
    ("d1_r57:d1_r57_c2:d1_b60", "d1_r57:d1_r57_c3:d1_b58", dict( count=9 )),
    ("d1_r57:d1_r57_c3:d1_b58", "d1_r57:d1_r57_c3:d1_b29", dict( count=90 )),
    ("d1_r57:d1_r57_c3:d1_b29", "d1_r57:d1_r57_c3:d1_b18", dict( count=81, cycles=405 )),
    ("d1_r57:d1_r57_c3:d1_b29", "d1_r57:d1_r57_c3:d1_b59", dict( count=9, cycles=45 )),
    ("d1_r57:d1_r57_c3:d1_b18", "d1_r57:d1_r57_c3:d1_b60", dict( count=81, cycles=1296 )),
    # Intra-call to L2 c3
    ("d1_r57:d1_r57_c3:d1_b60", "d1_r57:d1_r57_c3:d1_b58", dict( count=81 )),
    # Intra-return from L2 c3
    ("d1_r57:d1_r57_c3:d1_b59", "d1_r57:d1_r57_c3:d1_b61", dict( count=81 )),
    ("d1_r57:d1_r57_c3:d1_b61", "d1_r57:d1_r57_c3:d1_b59", dict( count=81 )),
    # End to L2 c3
    ("d1_r57:d1_r57_c3:d1_b59", "d1_r57:d1_r57_c2:d1_b61", dict( count=9 )),
    ("d1_r57:d1_r57_c2:d1_b61", "d1_r57:d1_r57_c2:d1_b59", dict( count=9 )),
    # Return from L2 l2
    ("d1_r57:d1_r57_c2:d1_b59", "d1_r73:d1_r73_c1:d1_b68", dict( count=9 )),
    ("d1_r73:d1_r73_c1:d1_b68", "d1_r73:d1_r73_c1:d1_b35", dict( count=9 )),
    ("d1_r73:d1_r73_c1:d1_b35", "d1_r73:d1_r73_c1:d1_b76", dict( count=9, cycles=36 )),
    # Intra-call L1 l1
    ("d1_r73:d1_r73_c1:d1_b76", "d1_r73:d1_r73_c1:d1_b74", dict( count=9 )),
    # Intra-return L1 l1
    ("d1_r73:d1_r73_c1:d1_b75", "d1_r73:d1_r73_c1:d1_b77", dict( count=9 )),
    ("d1_r73:d1_r73_c1:d1_b77", "d1_r73:d1_r73_c1:d1_b75", dict( count=9 )),
    # End call to L1 l1
    ("d1_r73:d1_r73_c1:d1_b75", "d1_r73:d1_r73_c0:d1_b77", dict( count=1 )),
    ("d1_r73:d1_r73_c0:d1_b77", "d1_r73:d1_r73_c0:d1_b75", dict( count=1 )),
    # End call to L1 c0
    ("d1_r73:d1_r73_c0:d1_b75", "d1_r2:d1_r2_c0:d1_b84", dict( count=1 )),
    ("d1_r2:d1_r2_c0:d1_b84", "d1_r2:d1_r2_c0:d1_b46", dict( count=1 )),
    ("d1_r2:d1_r2_c0:d1_b46", "d1_r2:d1_r2_c0:d1_b4", dict( count=1, cycles=9 )),
] )

# Data about call.xml

call_icfg = nx.DiGraph()
call_icfg.add_node( "d1_r2" )
call_icfg.add_node( "d1_r20" )

call_kernel_cfg = nx.DiGraph()
call_kernel_cfg.add_edges_from( [
    # kernel
    ('d1_b21', 'd1_b23'),
    ('d1_b23', 'd1_b25'),
    ('d1_b25', 'd1_b26'),
    # call f
    # ('d1_b25', 'd1_b3'),
    ('d1_b26', 'd1_b42'),
    ('d1_b42', 'd1_b22'),

] )

call_icfg.nodes[ "d1_r20" ][ "cfg" ] = call_kernel_cfg

call_f_cfg = nx.DiGraph()
call_f_cfg.add_edges_from( [
    # f
    ('d1_b3', 'd1_b5'),
    # end to kernel
    # ('d1_b4', 'd1_b26'),
    ('d1_b5', 'd1_b4'),
] )

call_icfg.nodes[ "d1_r2" ][ "cfg" ] = call_f_cfg
call_icfg.add_edge( "d1_r20", "d1_r2", callIds=[ "d1_b25" ] )


call_wcet_path = nx.DiGraph()

call_wcet_path.add_node( "d1_r20:d1_r20_c0:d1_b21", start=True, start_routine=True )
call_wcet_path.add_node( "d1_r2:d1_r2_c0:d1_b3", start_routine=True )

call_wcet_path.add_node( "d1_r20:d1_r20_c0:d1_b22", end=True, end_routine=True )
call_wcet_path.add_node( "d1_r2:d1_r2_c0:d1_b4", end_routine=True )

call_wcet_path.add_edges_from( [
    ("d1_r20:d1_r20_c0:d1_b21", "d1_r20:d1_r20_c0:d1_b23", dict( count=1 )),
    ("d1_r20:d1_r20_c0:d1_b23", "d1_r20:d1_r20_c0:d1_b25", dict( count=1, cycles=20 )),
    # Call to f
    ("d1_r20:d1_r20_c0:d1_b25", "d1_r2:d1_r2_c0:d1_b3", dict( count=1 )),
    ("d1_r2:d1_r2_c0:d1_b3", "d1_r2:d1_r2_c0:d1_b5", dict( count=1 )),
    ("d1_r2:d1_r2_c0:d1_b5", "d1_r2:d1_r2_c0:d1_b4", dict( count=1, cycles=20 )),
    # Return from f
    ("d1_r2:d1_r2_c0:d1_b4", "d1_r20:d1_r20_c0:d1_b26", dict( count=1 )),
    ("d1_r20:d1_r20_c0:d1_b26", "d1_r20:d1_r20_c0:d1_b42", dict( count=1 )),
    ("d1_r20:d1_r20_c0:d1_b42", "d1_r20:d1_r20_c0:d1_b22", dict( count=1, cycles=20 )),
] )

# Data about call_in_loop.xml

call_in_loop_icfg = nx.DiGraph()
call_in_loop_icfg.add_node( "d1_r2" )
call_in_loop_icfg.add_node( "d1_r20" )

call_in_loop_kernel_cfg = nx.DiGraph()
call_in_loop_kernel_cfg.add_edges_from( [
    # kernel
    ('d1_b21', 'd1_b23'),
    ('d1_b23', 'd1_b84'),
    ('d1_b61', 'd1_b22'),
    ('d1_b84', 'd1_b85'),
    ('d1_b84', 'd1_b75'),
    ('d1_b85', 'd1_b61'),
    # kernel.L1
    ('d1_b75', 'd1_b55'),
    ('d1_b76', 'd1_b78'),
    ('d1_b76', 'd1_b85'),
    ('d1_b35', 'd1_b36'),
    ('d1_b36', 'd1_b37'),
    # call to f
    # ('d1_b36', 'd1_b3'),
    ('d1_b37', 'd1_b46'),
    ('d1_b46', 'd1_b77'),
    ('d1_b55', 'd1_b35'),
    ('d1_b55', 'd1_b76'),
    ('d1_b77', 'd1_b78'),
    ('d1_b77', 'd1_b75'),
    ('d1_b78', 'd1_b76'),
] )

call_in_loop_icfg.nodes[ "d1_r20" ][ "cfg" ] = call_in_loop_kernel_cfg

call_in_loop_f_cfg = nx.DiGraph()
call_in_loop_f_cfg.add_edges_from( [
    # f
    ('d1_b3', 'd1_b5'),
    # end to kernel
    # ('d1_b4', 'd1_b37'),
    ('d1_b5', 'd1_b4'),
] )

call_in_loop_icfg.nodes[ "d1_r2" ][ "cfg" ] = call_in_loop_f_cfg
call_in_loop_icfg.add_edge( "d1_r20", "d1_r2", callIds=[ "d1_b36" ] )

call_in_loop_wcet_path = nx.DiGraph()

call_in_loop_wcet_path.add_node( "d1_r20:d1_r20_c0:d1_b21", start=True, start_routine=True )
call_in_loop_wcet_path.add_node( "d1_r2:d1_r2_c0:d1_b3", start_routine=True )
call_in_loop_wcet_path.add_node( "d1_r2:d1_r2_c1:d1_b3", start_routine=True )
call_in_loop_wcet_path.add_node( "d1_r74:d1_r74_c0:d1_b75", start_routine=True )
call_in_loop_wcet_path.add_node( "d1_r74:d1_r74_c1:d1_b75", start_routine=True )

call_in_loop_wcet_path.add_node( "d1_r20:d1_r20_c0:d1_b22", end=True, end_routine=True )
call_in_loop_wcet_path.add_node( "d1_r2:d1_r2_c0:d1_b4", end_routine=True )
call_in_loop_wcet_path.add_node( "d1_r2:d1_r2_c1:d1_b4", end_routine=True )
call_in_loop_wcet_path.add_node( "d1_r74:d1_r74_c0:d1_b76", end_routine=True )
call_in_loop_wcet_path.add_node( "d1_r74:d1_r74_c1:d1_b76", end_routine=True )

call_in_loop_wcet_path.add_edges_from( [
    ("d1_r20:d1_r20_c0:d1_b21", "d1_r20:d1_r20_c0:d1_b23", dict( count=1 )),
    ("d1_r20:d1_r20_c0:d1_b23", "d1_r20:d1_r20_c0:d1_b84", dict( count=1, cycles=17 )),
    # Call to kernel.L1 c0
    ("d1_r20:d1_r20_c0:d1_b84", "d1_r74:d1_r74_c0:d1_b75", dict( count=1 )),
    ("d1_r74:d1_r74_c0:d1_b75", "d1_r74:d1_r74_c0:d1_b55", dict( count=1 )),
    ("d1_r74:d1_r74_c0:d1_b55", "d1_r74:d1_r74_c0:d1_b35", dict( count=1, cycles=6 )),
    ("d1_r74:d1_r74_c0:d1_b35", "d1_r74:d1_r74_c0:d1_b36", dict( count=1, cycles=8 )),
    # Call to f
    ("d1_r74:d1_r74_c0:d1_b36", "d1_r2:d1_r2_c0:d1_b3", dict( count=1 )),
    ("d1_r2:d1_r2_c0:d1_b3", "d1_r2:d1_r2_c0:d1_b5", dict( count=1 )),
    ("d1_r2:d1_r2_c0:d1_b5", "d1_r2:d1_r2_c0:d1_b4", dict( count=1, cycles=20 )),
    # Return from f
    ("d1_r2:d1_r2_c0:d1_b4", "d1_r74:d1_r74_c0:d1_b37", dict( count=1 )),
    ("d1_r74:d1_r74_c0:d1_b37", "d1_r74:d1_r74_c0:d1_b46", dict( count=1 )),
    ("d1_r74:d1_r74_c0:d1_b46", "d1_r74:d1_r74_c0:d1_b77", dict( count=1, cycles=12 )),
    # Call to kernel.L1 l1
    ("d1_r74:d1_r74_c0:d1_b77", "d1_r74:d1_r74_c1:d1_b75", dict( count=1 )),
    ("d1_r74:d1_r74_c1:d1_b75", "d1_r74:d1_r74_c1:d1_b55", dict( count=10 )),
    ("d1_r74:d1_r74_c1:d1_b55", "d1_r74:d1_r74_c1:d1_b35", dict( count=9, cycles=45 )),
    ("d1_r74:d1_r74_c1:d1_b55", "d1_r74:d1_r74_c1:d1_b76", dict( count=1, cycles=5 )),
    ("d1_r74:d1_r74_c1:d1_b35", "d1_r74:d1_r74_c1:d1_b36", dict( count=9, cycles=72 )),
    # Call to f
    ("d1_r74:d1_r74_c1:d1_b36", "d1_r2:d1_r2_c1:d1_b3", dict( count=9 )),
    ("d1_r2:d1_r2_c1:d1_b3", "d1_r2:d1_r2_c1:d1_b5", dict( count=9 )),
    ("d1_r2:d1_r2_c1:d1_b5", "d1_r2:d1_r2_c1:d1_b4", dict( count=9, cycles=180 )),
    # Return from f
    ("d1_r2:d1_r2_c1:d1_b4", "d1_r74:d1_r74_c1:d1_b37", dict( count=9 )),
    ("d1_r74:d1_r74_c1:d1_b37", "d1_r74:d1_r74_c1:d1_b46", dict( count=9 )),
    ("d1_r74:d1_r74_c1:d1_b46", "d1_r74:d1_r74_c1:d1_b77", dict( count=9, cycles=108 )),
    # Intra-call to kernel.L1 l1
    ("d1_r74:d1_r74_c1:d1_b77", "d1_r74:d1_r74_c1:d1_b75", dict( count=9 )),
    # Intra-return from kernel.L1 l1
    ("d1_r74:d1_r74_c1:d1_b76", "d1_r74:d1_r74_c1:d1_b78", dict( count=9 )),
    ("d1_r74:d1_r74_c1:d1_b78", "d1_r74:d1_r74_c1:d1_b76", dict( count=9 )),
    # Return from kernel.L1 l1
    ("d1_r74:d1_r74_c1:d1_b76", "d1_r74:d1_r74_c0:d1_b78", dict( count=1 )),
    ("d1_r74:d1_r74_c0:d1_b78", "d1_r74:d1_r74_c0:d1_b76", dict( count=1 )),
    # Return from kernel.L1 c0
    ("d1_r74:d1_r74_c0:d1_b76", "d1_r20:d1_r20_c0:d1_b85", dict( count=1 )),
    ("d1_r20:d1_r20_c0:d1_b85", "d1_r20:d1_r20_c0:d1_b61", dict( count=1 )),
    ("d1_r20:d1_r20_c0:d1_b61", "d1_r20:d1_r20_c0:d1_b22", dict( count=1, cycles=11 )),
] )

# Data about multi_calls.xml

multi_calls_icfg = nx.DiGraph()
multi_calls_icfg.add_node( "d1_r2" )
multi_calls_icfg.add_node( "d1_r20" )

multi_calls_kernel_cfg = nx.DiGraph()
multi_calls_kernel_cfg.add_edges_from( [
    # kernel
    ('d1_b21', 'd1_b23'),
    ('d1_b23', 'd1_b25'),
    ('d1_b25', 'd1_b26'),
    # call kernel
    # ('d1_b25', 'd1_b3'),
    ('d1_b26', 'd1_b42'),
    ('d1_b42', 'd1_b43'),
    ('d1_b43', 'd1_b44'),
    # call kernel
    # ('d1_b43', 'd1_b3'),
    ('d1_b44', 'd1_b53'),
    ('d1_b53', 'd1_b54'),
    ('d1_b54', 'd1_b55'),
    # call kernel
    # ('d1_b54', 'd1_b3'),
    ('d1_b55', 'd1_b64'),
    ('d1_b64', 'd1_b22'),
] )

multi_calls_icfg.nodes[ "d1_r20" ][ "cfg" ] = multi_calls_kernel_cfg

multi_calls_f_cfg = nx.DiGraph()
multi_calls_f_cfg.add_edges_from( [
    # f
    ('d1_b3', 'd1_b5'),
    # end to kernel
    # ('d1_b4', 'd1_b26'),
    # ('d1_b4', 'd1_b44'),
    # ('d1_b4', 'd1_b55'),
    ('d1_b5', 'd1_b4'),
] )

multi_calls_icfg.nodes[ "d1_r2" ][ "cfg" ] = multi_calls_f_cfg
multi_calls_icfg.add_edge( "d1_r20", "d1_r2", callIds=[ "d1_b25", "d1_b40", "d1_b51" ] )

multi_calls_wcet_path = nx.DiGraph()

multi_calls_wcet_path.add_node( "d1_r20:d1_r20_c0:d1_b21", start=True, start_routine=True )
multi_calls_wcet_path.add_node( "d1_r2:d1_r2_c0:d1_b3", start_routine=True )
multi_calls_wcet_path.add_node( "d1_r2:d1_r2_c1:d1_b3", start_routine=True )
multi_calls_wcet_path.add_node( "d1_r2:d1_r2_c2:d1_b3", start_routine=True )

multi_calls_wcet_path.add_node( "d1_r20:d1_r20_c0:d1_b22", end=True, end_routine=True )
multi_calls_wcet_path.add_node( "d1_r2:d1_r2_c0:d1_b4", end_routine=True )
multi_calls_wcet_path.add_node( "d1_r2:d1_r2_c1:d1_b4", end_routine=True )
multi_calls_wcet_path.add_node( "d1_r2:d1_r2_c2:d1_b4", end_routine=True )

multi_calls_wcet_path.add_edges_from( [
    ("d1_r20:d1_r20_c0:d1_b21", "d1_r20:d1_r20_c0:d1_b23", dict( count=1 )),
    ("d1_r20:d1_r20_c0:d1_b23", "d1_r20:d1_r20_c0:d1_b25", dict( count=1, cycles=20 )),
    # Call to f c0
    ("d1_r20:d1_r20_c0:d1_b25", "d1_r2:d1_r2_c0:d1_b3", dict( count=1 )),
    ("d1_r2:d1_r2_c0:d1_b3", "d1_r2:d1_r2_c0:d1_b5", dict( count=1 )),
    ("d1_r2:d1_r2_c0:d1_b5", "d1_r2:d1_r2_c0:d1_b4", dict( count=1, cycles=20 )),
    # Return from f c0
    ("d1_r2:d1_r2_c0:d1_b4", "d1_r20:d1_r20_c0:d1_b26", dict( count=1 )),
    ("d1_r20:d1_r20_c0:d1_b26", "d1_r20:d1_r20_c0:d1_b42", dict( count=1 )),
    ("d1_r20:d1_r20_c0:d1_b42", "d1_r20:d1_r20_c0:d1_b43", dict( count=1, cycles=8 )),
    # Call to f l1
    ("d1_r20:d1_r20_c0:d1_b43", "d1_r2:d1_r2_c1:d1_b3", dict( count=1 )),
    ("d1_r2:d1_r2_c1:d1_b3", "d1_r2:d1_r2_c1:d1_b5", dict( count=1 )),
    ("d1_r2:d1_r2_c1:d1_b5", "d1_r2:d1_r2_c1:d1_b4", dict( count=1, cycles=20 )),
    # Return from f l1
    ("d1_r2:d1_r2_c1:d1_b4", "d1_r20:d1_r20_c0:d1_b44", dict( count=1 )),
    ("d1_r20:d1_r20_c0:d1_b44", "d1_r20:d1_r20_c0:d1_b53", dict( count=1 )),
    ("d1_r20:d1_r20_c0:d1_b53", "d1_r20:d1_r20_c0:d1_b54", dict( count=1, cycles=8 )),
    # Call to f l2
    ( "d1_r20:d1_r20_c0:d1_b54", "d1_r2:d1_r2_c2:d1_b3", dict( count=1 ) ),
    ("d1_r2:d1_r2_c2:d1_b3", "d1_r2:d1_r2_c2:d1_b5", dict( count=1 )),
    ("d1_r2:d1_r2_c2:d1_b5", "d1_r2:d1_r2_c2:d1_b4", dict( count=1, cycles=20 )),
    # Return from f l2
    ("d1_r2:d1_r2_c2:d1_b4", "d1_r20:d1_r20_c0:d1_b55", dict( count=1 )),
    ("d1_r20:d1_r20_c0:d1_b55", "d1_r20:d1_r20_c0:d1_b64", dict( count=1 )),
    ("d1_r20:d1_r20_c0:d1_b64", "d1_r20:d1_r20_c0:d1_b22", dict( count=1, cycles=15 )),
] )
